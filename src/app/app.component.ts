import { Component } from '@angular/core';
import { trigger, state, style, animate, transition, keyframes } from '@angular/animations';
import * as $ from 'jquery';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    // the fade-in/fade-out animation.
    trigger('fadeAnimation', [

      // the "in" style determines the "resting" state of the element when it is visible.
      // state('in', style({ opacity: 1 })),

      // fade in when created. this could also be written as transition('void => *')
      transition(':enter', [style({ opacity: 0 }), animate(1000)]),

      // fade out when destroyed. this could also be written as transition('void => *')
      transition(':leave', animate(500, style({ opacity: 0 })))
    ])
  ]
})
export class AppComponent {
  title = 'app';
  todoTitle = '';
  todos = [
    { title: 'test1', done: false },
    { title: 'test2', done: true },
    { title: 'test3', done: false },
  ];

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
  }
  removeTodo(todo, event) {
    event.stopPropagation();
    console.log(event);
    // search for index of todo
    let todoIndex = this.todos.indexOf(todo);
    console.log(todoIndex);
    // splice todo
    this.todos.splice(todoIndex, 1);
  }

  markDone(todo) {
    // search for todo titile
    let todoIndex = this.todos.indexOf(todo);
    // set todo done to true/false
    this.todos[todoIndex].done = !this.todos[todoIndex].done;
  }

  // when enter is pressed
  // add todo
  onKeydown(event) {
    if (event.key === "Enter") {
      // add todo to the array
      let newTodo = {
        title: this.todoTitle,
        done: false
      }
      this.todos.push(newTodo);
      // clear input string
      this.todoTitle = '';
    }
  }

  toggleInput() {
    // hide input
    $('input[type="text"]').fadeToggle();
    // $('.fa-plus').toggleClass('rotatePlus');
    // $('.fa-plus').css({
    //   'transform': 'rotate(46deg)',
    //   'transition-duration': '0.5s'
    // })
  }

  onSwipeRight(event) {
    console.log(event);
  }
}
